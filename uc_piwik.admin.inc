<?php

function uc_piwik_settings_form() {
  $form = array();

  $form['uc_piwik_product_pages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable e-commerce tracking for product pages.'),
    '#summary callback' => 'summarize_checkbox',
    '#summary arguments' => array(
      t('Enabled.'),
      t('Disabled.'),
    ),
    '#default_value' => variable_get('uc_piwik_product_pages', TRUE),
  );
  
  $form['uc_piwik_cart_updates'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable e-commerce tracking for cart updates.'),
    '#summary callback' => 'summarize_checkbox',
    '#summary arguments' => array(
      t('Enabled.'),
      t('Disabled.'),
    ),
    '#default_value' => variable_get('uc_piwik_cart_updates', TRUE),
  );
  
  $form['uc_piwik_category_pages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable e-commerce tracking for category pages.'),
    '#summary callback' => 'summarize_checkbox',
    '#summary arguments' => array(
      t('Enabled.'),
      t('Disabled.'),
    ),
    '#default_value' => variable_get('uc_piwik_category_pages', TRUE),
  );

  $form['uc_piwik_checkout_complete'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable e-commerce tracking for completed checkouts.'),
    '#summary callback' => 'summarize_checkbox',
    '#summary arguments' => array(
      t('Enabled.'),
      t('Disabled.'),
    ),
    '#default_value' => variable_get('uc_piwik_checkout_complete', TRUE),
  );   
  
  return system_settings_form($form);
}
